<?php

	class Mini_prj_model extends CI_Model{

		public function __construct(){

			$this->load->database();
			
		}

		public function get_data(){

				 $this->load->database();
				 $this->db->join('categories','categories.Id = posts.category_id');

				 $query = $this->db->get('posts');

				 return($query->result_array());

		}

		public function get_data_post($slug){

				 $this->load->database();
				 $this->db->join('categories','categories.Id = posts.category_id');

				 $query = $this->db->get_where('posts',array('slug'=> $slug));

				 return($query->result_array());

		}


		public function create_post($data1){
			print_r($data1);
				$this->load->database();
				//$this->db->order_by('posts.Id','DESC');
				$slug = url_title($this->input->post('title'));

				$data = array(

				 		"title" => $this->input->post('title'),
				 		"slug" => $slug,
				 		"body" => $this->input->post('body'),
				 		"user_id" => $this->session->userdata('user_id'),
				 		"category_id" => $this->input->post('categorie_id'),
				 		"post_image" => $data1[2]

				 );
				
					return $this->db->insert("posts",$data);
					
		}

		public function delete_post($Id) { 

				$this->load->database();
				
		         if ($this->db->delete("posts", "Id = ".$Id)) { 
		            return true; 
		         } 

      }


		public function edit_post($slug){

				 $this->load->database();
				 
				 $query =$this->db->get_where('posts', array('slug =' => $slug));

				 return($query->result_array());

		}

		public function update_post(){

					$this->load->database();

					$slug = url_title($this->input->post('title'));

					$data = array(

									"title" => $this->input->post('title'),
									 "slug" => $slug,
									 "body" => $this->input->post('body'),
									 "category_id" => $this->input->post('categorie_id')

					);
					//var_dump($data);
					$this->db->where('id',$this->input->post('Id'));
					return $this->db->update("posts",$data);
		}

		public function get_categories(){

					$this->load->database();
					$this->db->order_by('name');
					$query = $this->db->get('categories');
					return $query->result_array();
		}

		//category =======================================================

		public function create_category(){

				$data = array(

									"name" => $this->input->post('Name')
						);

				return $this->db->insert('categories',$data);

		}

		public function get_posts_by_id($id)
		{

				$this->db->order_by('posts.Id','DESC');

				$this->load->database();

				$this->db->join('categories','categories.Id = posts.category_id');

				$query = $this->db->get_where('posts',array('category_id' => $id));

				return $query->result_array();


		}

		//Comment =======================================================
		
		public function create_comment($id){

			$data = array(
				"post_id" => $id,
				"name" => $this->input->post('name'),
				"email" => $this->input->post('email'),
				"body" => $this->input->post('body'),
				);

			return $this->db->insert("comments",$data);
		}

			public function get_comments($post_id){
				var_dump($post_id);
				$query = $this->db->get_where('comments', array('post_id' => $post_id));
				var_dump($query->result_array());
				 //return($query->result_array());
			}
	}
?>