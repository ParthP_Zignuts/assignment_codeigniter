<?php 
   class Emp_Model extends CI_Model {
	
      function __construct() { 
         parent::__construct(); 
      } 
      

      public function insert($data) { 
         if ($this->db->insert("emp_info", $data)) { 
            return true; 
         } 
      } 
   
      public function delete($Id) { 
         if ($this->db->delete("emp_info", "Id = ".$Id)) { 
            return true; 
         } 
      } 
   
      public function update($data,$old_roll_no) { 
         $this->db->set($data); 
         $this->db->where("roll_no", $old_roll_no); 
         $this->db->update("stud", $data); 
      } 
   } 
?> 