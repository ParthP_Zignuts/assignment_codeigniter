
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Mini Project </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>
<body>





<nav class="navbar navbar-expand-sm bg-primary navbar-dark">
  <ul class="navbar-nav">
    <li class="nav-item active">
      <a class="nav-link" href="<?php echo base_url(); ?>Mini_project/view/">Home</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo base_url(); ?>Mini_project/view/about">About</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo base_url(); ?>Mini_project/view/post">Blog</a>
    </li>
   
    

  <?php if(!$this->session->userdata('user_logedin')): ?>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo base_url(); ?>User/login">Login</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo base_url(); ?>User/register">Register</a>
    </li>
  <?php endif; ?>


  <?php if(!$this->session->userdata('user_logedin')): ?>

    <li class="nav-item">
      <a class="nav-link" href="<?php echo base_url(); ?>Mini_project/view/create_post">CreatePost</a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="<?php echo base_url(); ?>Mini_project/category">Category</a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="<?php echo base_url(); ?>Mini_project/create_category">CreateCategory</a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="<?php echo base_url(); ?>User/logout">Logout</a>
    </li>

 <?php endif; ?>

    <li class="nav-item">
      <a class="nav-link" href="<?php echo base_url(); ?>Mini_project/view/contact">Contact</a>
    </li>

    

    <!--<li class="nav-item">
      <a class="nav-link disabled" href="#">Disabled</a>
    </li>-->
  </ul>
</nav>

<div class="container">

<!-- Flash message-->

<?php if($this->session->flashdata('user_registered')):?>

    <?php echo "<p class='alert alert-sucess'>".$this->session->flashdata('user_registered')."</p>"; ?>

<?php endif;?>

<?php if($this->session->flashdata('post_created')):?>

    <?php echo "<p class='alert alert-sucess'>".$this->session->flashdata('post
    ')."</p>"; ?>

<?php endif;?>

<?php if($this->session->flashdata('category_created')):?>

    <?php echo "<p class='alert alert-sucess'>".$this->session->flashdata('category_created')."</p>"; ?>

<?php endif;?>

<?php if($this->session->flashdata('post_updated')):?>

    <?php echo "<p class='alert alert-sucess'>".$this->session->flashdata('post_updated
    ')."</p>"; ?>

<?php endif;?>

<?php if($this->session->flashdata('post_deleted')):?>

    <?php echo "<p class='alert alert-sucess'>".$this->session->flashdata('post_deleted
    ')."</p>"; ?>

<?php endif;?>

<?php if($this->session->flashdata('login_failed')):?>

    <?php echo "<p class='alert alert-danger'>".$this->session->flashdata('login_failed
    ')."</p>"; ?>

<?php endif;?>

<?php if($this->session->flashdata('user_logedin')):?>

    <?php echo "<p class='alert alert-success'>".$this->session->flashdata('user_logedin
    ')."</p>"; ?>

<?php endif;?>

<?php if($this->session->flashdata('logged_out')):?>

    <?php echo "<p class='alert alert-success'>".$this->session->flashdata('logged_out
    ')."</p>"; ?>

<?php endif;?>