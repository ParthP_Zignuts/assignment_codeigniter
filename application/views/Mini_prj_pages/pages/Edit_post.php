<h2>Edit Post</h2>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Edit Post</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  
 <?php echo validation_errors(); ?>

 <?php echo form_open('Mini_project/update_post'); ?>

    <div class="form-group">
      <input type="hidden" class="form-control"  name="Id" value="<?php echo $post[0]['Id'];?>">
      <label class="control-label col-sm-2" for="">Title</label>
      <div class="col-sm-10">
        <input type="text" class="form-control"  name="title" value="<?php echo $post[0]['title'];?>">
      </div>
    </div>
    </br></br>
    <div class="form-group">
      <label class="control-label col-sm-2" for="pwd">Body</label>
      <div class="col-sm-10">          
        <input class="form-control"  name="body" value="<?php echo $post[0]['body'];?>"> </input>
      </div>
    </div>

     </br></br>
     
     <div class="form-group">
      <label class="control-label col-sm-2" for="">Categories</label>
      <div class="col-sm-10">
        <select class="form-control"  name="categorie_id"> 
  
            <?php foreach($categories as $categorie): ?>

                <option value="<?php echo $categorie['Id'];?>"><?php echo $categorie['name']; ?></option>

            <?php endforeach; ?>
        
        </select> 
      </div>
    </div><br><br>

    <div class="form-group">    	    
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-default">Submit</button>
      </div>
    </div>
  </form>
</div>

</body>
</html>
