<h3><?php foreach($post as $data): ?></h3>

	<h5><?= $data['title']; ?></h5>
	<div class="row">
		<div class="col-md-3">
			
			<img src="<?php echo site_url();?>assets/images/post/<?php echo $data['post_image'];?>"	>

		</div>
		<div class="col-md-9">
					
				<small class="post"><?= $data['created_at']; ?></small><br>
		
				<?php echo word_limiter($data['body']);?><br><br>
					
				<p><a class="btn-default"  href="<?php echo site_url('/Mini_project/view1/'.$data['slug']); ?>">Read More</a></p>

		</div>
	</div>

	

<?php endforeach; ?>

