<?php
	
	//echo "<center><h1>This is a Employee Skills page</h1></center>";

	//print_r($emp_data);
?>

<!DOCTYPE html>
<html>
<head>
	<title>Employee Skills List</title>
</head>
<body>
<center>
	<h1>Employee Skills List</h1>

		<table border="2px solid black">
			
			<tr>
				<td>Id</td>
				<td>Name</td>
				<td>Skills</td>
			</tr>

			<?php foreach($emp_data as $user): ?>

			<tr>
				<td><?= $user['Id']?></td>
				<td><?= $user['Name']?></td>
				<td><?= $user['Skills']?></td>
			</tr>
			
			<?php endforeach;?>

		</table>
</center>

</body>
</html>