<!DOCTYPE html>
<html>
<head>
	<title>Users List</title>
</head>
<body>
<center>
	<h1>Display Data Use Codeigniter framework (join 4 tables)</h1>

		<table border="2px solid black">
			
			<tr>
				<td>Id</td>
				<td>First Name</td>
				<td>City</td>
				<td>Designation</td>
				<td>Salary</td>
				<td>Skills</td>
			</tr>

			<?php foreach($result as $user): ?>

			<tr>
				<td><?= $user['Id']?></td>
				<td><?= $user['Name']?></td>
				<td><?= $user['City']?></td>
				<td><?= $user['Designation']?></td>
				<td><?= $user['Salary']?></td>
				<td><?= $user['Skills']?></td>
			</tr>
			
			<?php endforeach;?>

		</table>
</center>

</body>
</html>