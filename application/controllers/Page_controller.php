<?php

	class Page_controller extends CI_Controller{ 

				function __construct() { 
					         parent::__construct(); 
					         $this->load->helper('url'); 
					         $this->load->helper('html'); 
					         $this->load->database(); 

				      } 

				      public function show() {

				        $data = $this->db->get("emp_info"); 
				        var_dump($data->result_array());				            
				         
				      } 

				public function add_emp_view() { 
					         $this->load->helper('form'); 
					         $this->load->view('Emp_add');
					      } 	

				public function add_emp() { 
					         $this->load->model('Emp_Model');
								
					         $data = array( 
					            'Id' => $this->input->post('Id'), 
					            'Name' => $this->input->post('Name'),
					            'City' => $this->input->post('City'), 
					            'Con_no' => $this->input->post('Con_no')

					         ); 
								
					         $this->Emp_Model->insert($data); 
					   
					         $query = $this->db->get("emp_info"); 
					         $data['emp_data'] = $query->result_array(); 
					         $this->load->view('pages/Info', $data);
					      } 

				 public function delete_emp() { 
				 	
					         $this->load->model('Emp_Model'); 
					         $Id = $this->uri->segment('3'); 
					         $this->Emp_Model->delete($Id); 
					   
					         $query = $this->db->get("emp_info"); 
					         $data['emp_data'] = $query->result_array(); 
					         $this->load->view('pages/Info',$data); 
					      } 

				public function update_student_view() { 
					         $this->load->helper('form'); 
					         $roll_no = $this->uri->segment('3'); 
					         $query = $this->db->get_where("stud",array("roll_no"=>$roll_no));
					         $data['records'] = $query->result(); 
					         $data['old_roll_no'] = $roll_no; 
					         $this->load->view('Stud_edit',$data); 
					      } 
					  
				public function update_student(){ 
					         $this->load->model('Stud_Model');
								
					         $data = array( 
					            'roll_no' => $this->input->post('roll_no'), 
					            'name' => $this->input->post('name') 
					         ); 
								
					         $old_roll_no = $this->input->post('old_roll_no'); 
					         $this->Stud_Model->update($data,$old_roll_no); 
								
					         $query = $this->db->get("stud"); 
					         $data['records'] = $query->result(); 
					         $this->load->view('Stud_view',$data); 
					      } 


				public function view($page = 'home')
				{
					
				        if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php'))
				        {
				                // Whoops, we don't have a page for that!
				                show_404();
				        }
				        
				       

				        $data['title'] = ucfirst($page); // Capitalize the first letter

				        $this->load->model('Page_model');
						$data['emp_data'] = $this->Page_model->getData();

						//print_r($data);

				        $this->load->view('templates/header', $data);
				        $this->load->view('pages/'.$page, $data);
				        $this->load->view('templates/footer', $data);


				     
				}
	}

?>