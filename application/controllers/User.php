<?php

	class User extends CI_Controller{

		public function __construct(){
			
			parent::__construct();
     		$this->load->library('form_validation');
     		$this->load->library('session');

		}

		//register user
		public function register(){

			
			

			$data['title'] = "Sign Up";

				$this->form_validation->set_rules('name','Name','required');
				$this->form_validation->set_rules('username','UserName','required|callback_check_username_exists');
			    $this->form_validation->set_rules('email','Email','required|callback_check_email_exists');
			    $this->form_validation->set_rules('password','Password','required');
				$this->form_validation->set_rules('password2','ConfirmPassword','matches[password]');

			if($this->form_validation->run() === FALSE ){
	
						$this->load->view('Mini_prj_pages/templats/header');
						$this->load->view('Mini_prj_pages/users/register',$data);
						$this->load->view('Mini_prj_pages/templats/footer');
					
			}else{

						//Encrypt Password===
						$enc_password = md5($this->input->post('password'));

						//set message
						$this->session->set_flashdata('user_registered','You are now Register and can Log in');	

						$this->User_model->register($enc_password);

						redirect('post');

			}
		}

		//login user
		//
		public function login(){

			
		$data['title'] = "Sign In";

			
				$this->form_validation->set_rules('username','UserName','required');
			    
			    $this->form_validation->set_rules('password','Password','required');
		
			if($this->form_validation->run() === FALSE ){
	
						$this->load->view('Mini_prj_pages/templats/header');
						$this->load->view('Mini_prj_pages/users/login',$data);
						$this->load->view('Mini_prj_pages/templats/footer');
					
			}else{

						//Get user name
						$username = $this->input->post('username');
						//Get encript password
						$password = md5($this->input->post('password'));

					
						


						$user_id = $this->User_model->login($username,$password);

						

					
						if($user_id){

							$user_data = array(
								$user_id => $user_id,
								$username =>$username,
								$loggedin => true
							);

							$this->session->set_userdata($user_data);

							$this->session->set_flashdata('user_logedin','You are now Logged in');	
							redirect('post');

						}else{

							$this->session->set_flashdata('login_failed','Login is Invalid..');	
							redirect('user/login');

						}

				}
		}

		//user logout
		

		public function logout(){
			
				//unset user data
				
				$this->session->unset_userdata('loggedin');
				$this->session->unset_userdata('user_id');
				$this->session->unset_userdata('username');

				$this->session->set_flashdata('logged_out','You are Logout');	

				redirect('user/login');

		}

		//check if username exist
		
		function check_username_exists($username){
				$this->form_validation->set_message('check_username_exists','That username Taken , Please choose a different one.');

				if($this->User_model->check_username_exists($username)){
						return true;
				}else{
						return false;
				}
		}
		//check if email exist
		
		function check_email_exists($email){
				$this->form_validation->set_message('check_email_exists','That email Taken , Please choose a different one.');

				if($this->User_model->check_email_exists($email)){
						return true;
				}else{
						return false;
				}
		}
	}

?>