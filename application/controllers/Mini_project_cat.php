<?php


	class Mini_project_cat extends CI_Controller{

		public function index(){

			$this->load->model('Mini_prj_cat_model','categorie_model');
		}

		public function create(){

			$data['title']="Create Categories";

			 $this->form_validation->set_rules('name','Name','required');
			 

				if($this->form_validation->run() === FALSE ){

						$this->load->view('Mini_prj_pages/templats/header');
						$this->load->view('Mini_prj_pages/pages/create_categorie',$data);
						$this->load->view('Mini_prj_pages/templats/footer');
					
				}else{

						$this->categorie_model->create_category();
						redirect('categories');

				}
		}

	}

?>