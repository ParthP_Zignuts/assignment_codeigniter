<?php

	class Mini_project extends CI_Controller{

		public function index($offset = 0){
			//pagination config
			$config['base_url'] = base_url().'Mini_project/index';
			$config['total_rows'] = $this->db->count_all('posts');
			$config['per_page'] = 3;
			$config['uri_segment'] = 3;

			//Init Pagination
			$this->pagination->initialize($config);
		}

		//for controlling pages 
		public function view($page = 'home'){
				
				if ( ! file_exists(APPPATH.'views/Mini_prj_pages/pages/'.$page.'.php'))
				        {
				               show_404();
				        }
				        else{

				        	$this->load->model('Mini_prj_model');

				        		$posts['post'] = $this->Mini_prj_model->get_data();

						    	$data['title'] = ucfirst($page);
 							
	 							$this->load->view('Mini_prj_pages/templats/header');
								$this->load->view('Mini_prj_pages/pages/'.$page,$posts);
								$this->load->view('Mini_prj_pages/templats/footer');
			}
		}

		//for display blog for post
		public function view1($slug = NULL ){

						$this->load->model('Mini_prj_model');
						$posts['post'] = $this->Mini_prj_model->get_data_post($slug);
						//var_dump($posts['post']);
						$post_id = $posts['post']['id'];

						
						$data['comment'] = $this->Mini_prj_model->get_comments($post_id);

						if(empty($posts['post'])){
							
								show_404();
						}

						$posts['title'] = $posts['post'];

								$this->load->view('Mini_prj_pages/templats/header');
								$this->load->view('Mini_prj_pages/pages/read_more',$posts);
								$this->load->view('Mini_prj_pages/templats/footer');


		}

		//for create a post 
		public function create_post(){

			if(!$this->session->userdata('user_logedin')){
				redirect('User/login');
			}


				$this->load->model('Mini_prj_model');

				$data['categories'] = $this->Mini_prj_model->get_categories();

				$this->form_validation->set_rules('title','Title','required');
				$this->form_validation->set_rules('body','Body','required');

				if($this->form_validation->run() === FALSE ){

						$this->load->view('Mini_prj_pages/templats/header');
						$this->load->view('Mini_prj_pages/pages/create_post',$data);
						$this->load->view('Mini_prj_pages/templats/footer');
					
				}else{

						$config['upload_path'] = '.assets/images/posts';
						$config['allowed_types'] = 'gif|png|jpg';
						$config['max_size'] = 2048;
						$config['max_width'] = 500;
						$config['max_height'] = 500;

						//var_dump($config);

						$this->load->library('upload',$config);

						if(!$this->upload->do_upload('postimage')){

								$errors = array('errors' => $this->upload->display_errors());
								$post_image = "noimage.jpg";

						}else{

								$upload = array('upload_data' => $this->upload->data());
								$post_image = $_FILES['postimage']['name'];
						}

						$data1 = array("1"=>$data,"2"=>$post_image);

						$this->Mini_prj_model->create_post($data1);

						//set message 
						$this->session->set_flashdata('post_created','Your post has been created');	

						redirect('post');
					}

		}

		public function delete_post($Id){
					
					if(!$this->session->userdata('user_logedin')){
							redirect('User/login');
					}


					$this->load->model('Mini_prj_model');
					$this->Mini_prj_model->delete_post($Id);

									//set message 
					$this->session->set_flashdata('post_deleted','Your post has been deleted');

					redirect('post');

		}

		public function edit_post($slug){


				if(!$this->session->userdata('user_logedin')){
					redirect('User/login');
				}

				$this->load->model('Mini_prj_model');
				$posts['categories'] = $this->Mini_prj_model->get_categories();
				$posts['post'] = $this->Mini_prj_model->edit_post($slug);
				
				if(empty($posts['post'])){
					
					show_404();
				}

			    $this->load->view('Mini_prj_pages/templats/header');
				$this->load->view('Mini_prj_pages/pages/Edit_post',$posts);
				$this->load->view('Mini_prj_pages/templats/footer');


		}

		public function update_post(){


				if(!$this->session->userdata('user_logedin')){
						redirect('User/login');
					}


				$this->load->model('Mini_prj_model');
				$this->Mini_prj_model->update_post();

				//set message 
				$this->session->set_flashdata('post_updated','Your post has been updated');

				redirect('post');
		}

		//Create Category ==========================================================
		public function create_category(){


			if(!$this->session->userdata('user_logedin')){
				redirect('User/login');
			}


			$data['title']="Create Categories";

			 $this->form_validation->set_rules('Name','Name','required');
			 

				if($this->form_validation->run() === FALSE ){

						$this->load->view('Mini_prj_pages/templats/header');
						$this->load->view('Mini_prj_pages/pages/create_categorie',$data);
						$this->load->view('Mini_prj_pages/templats/footer');
					
				}else{

						$this->load->model('Mini_prj_model');
						$this->Mini_prj_model->create_category();

						//set message 
						$this->session->set_flashdata('category_created','Your category has been created');

						redirect('Mini_project/category');

				}
		}

		public function category(){

						$this->load->model('Mini_prj_model');

						$data['title'] = "Categories";

						$data['categories'] = $this->Mini_prj_model->get_categories();

						$this->load->view('Mini_prj_pages/templats/header');
						$this->load->view('Mini_prj_pages/pages/categories',$data);
						$this->load->view('Mini_prj_pages/templats/footer');
					

			}

	    public function posts($id){

				    	$this->load->model('Mini_prj_model');

				    	$data['title'] = $this->Mini_prj_model->get_categories($id);

				    	$data['posts'] = $this->Mini_prj_model->get_posts_by_id($id);

				    	$this->load->view('Mini_prj_pages/templats/header');
						$this->load->view('Mini_prj_pages/pages/index',$data);
						$this->load->view('Mini_prj_pages/templats/footer');

	    }

		//Create Comment ==========================================================
		
	    public function create_comment($id){

	    	$this->load->model('Mini_prj_model');

	    	$slug = $this->input->post('slug');

	    	$data['post'] = $this->Mini_prj_model->get_data($slug);

			    	$this->form_validation->set_rules('name','Name','required');
			    	$this->form_validation->set_rules('email','Email','required');
			    	//$this->form_validation->set_rules('email','Email','valid_email');
					$this->form_validation->set_rules('body','Body','required');

			if($this->form_validation->run() === FALSE ){

						$this->load->view('Mini_prj_pages/templats/header');
						$this->load->view('Mini_prj_pages/pages/read_more',$data);
						$this->load->view('Mini_prj_pages/templats/footer');
					
			}else{

						
						$this->Mini_prj_model->create_comment($id);
						echo "yes";
						//redirect('Mini_project/post/'.$slug);

			}

	    }

	}

?>