<?php

	Class Test_controller extends CI_Controller{

		public function index(){
			//echo "this is a index function..";
			$this->load->view('Test_view');
		}

		public function getdata(){

			$this->load->model('Test_model');
			$name['full_name'] = $this->Test_model->getData();
			///print_r($name);
			$this->load->view('Test_view',$name);

		}

	}
	
?>